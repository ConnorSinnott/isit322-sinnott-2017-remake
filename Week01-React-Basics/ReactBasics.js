/**
 * Created by spectre on 4/27/17.
 */
import React from 'react';

export class ReactBasics extends React.Component {

    render() {
        return <h1> An H1 element in a React Component </h1>
    }

}