import React from 'react';
import ReactDOM from 'react-dom';
import {ReactBasics} from './ReactBasicsStatelessFunctional';

ReactDOM.render(<ReactBasics/>, document.getElementById('root'));