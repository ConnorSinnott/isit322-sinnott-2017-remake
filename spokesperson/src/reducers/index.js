/**
 * Created by spectre on 6/6/17.
 */
import {combineReducers} from 'redux';
import SpokespersonReducer from './reducer_Spokesperson';

export default combineReducers({
    spokespersonStore: SpokespersonReducer,
})