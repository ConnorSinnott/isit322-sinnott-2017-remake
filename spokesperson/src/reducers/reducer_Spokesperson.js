/**
 * Created by spectre on 6/6/17.
 */
import {verify, deny, noComment} from '../actions/actions_Spokesperson';
export default (state = {statement: 'No comment.'}, action) => {
    switch (action.type) {
        case verify.type:
            return {...state, statement: 'We stand by it. In fact, we invented it.'};
        case deny.type:
            return {...state, statement: 'We deny everything. We have never heard of it.'};
        case noComment.type:
            return {...state, statement: 'No comment.'};
        default:
            return state;
    }
}