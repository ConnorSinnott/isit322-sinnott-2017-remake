/**
 * Created by spectre on 6/6/17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {verify, deny, noComment} from './actions/actions_Spokesperson';

class SimpleRedux extends Component {

    render() {
        return (
            <div>

                <button onClick={() => {
                    this.props.dispatch(verify)
                }}> Verify
                </button>

                <button onClick={() => {
                    this.props.dispatch(deny)
                }}> Deny
                </button>

                <button onClick={() => {
                    this.props.dispatch(noComment)
                }}> No Comment
                </button>

                <p>{this.props.spokespersonStore.statement}</p>

            </div>
        );
    }

}

function mapStateToProps(state) {
    return {spokespersonStore: state.spokespersonStore};
}

export default connect(mapStateToProps)(SimpleRedux);