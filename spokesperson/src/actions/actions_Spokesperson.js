/**
 * Created by spectre on 6/6/17.
 */
module.exports = {
    verify: {
        type: 'VERIFY'
    },
    deny: {
        type: 'DENY'
    },
    noComment: {
        type: 'NO COMMENT'
    }
};