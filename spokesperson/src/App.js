import React, {Component} from 'react';
import logo from './logo.svg';
import {connect} from 'react-redux'
import './App.css';
import {verify} from './actions/actions_Spokesperson';
import SimpleRedux from './SimpleRedux';

class App extends Component {

    constructor(props) {
        super(props);
        props.dispatch(verify);
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <SimpleRedux/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        spokespersonStore: state.spokespersonStore,
    };
}

export default connect(mapStateToProps)(App);