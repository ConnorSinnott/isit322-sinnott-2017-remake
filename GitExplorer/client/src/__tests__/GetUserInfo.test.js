import React from 'react';
import ReactDOM from 'react-dom';
import GetUserInfo from '../components/GetUserInfo';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<GetUserInfo />, div);
});
