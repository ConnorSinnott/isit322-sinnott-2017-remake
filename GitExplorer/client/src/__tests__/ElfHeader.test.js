/**
 * Created by spectre on 5/9/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import ElfHeader from '../components/ElfHeader';
import {shallow} from 'enzyme';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ElfHeader />, div);
});

it('renders and reads H2 text', () => {
    const wrapper = shallow(<ElfHeader/>);
    const welcome = <h2>Welcome to React</h2>;
    expect(wrapper.contains(welcome)).toEqual(true);
});