/**
 * Created by spectre on 5/9/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import DataMaven from '../components/DataMaven';
import {shallow} from 'enzyme';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<DataMaven />, div);
});
