/**
 * Created by spectre on 5/9/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import GistLister from '../components/GistLister';
import {shallow} from 'enzyme';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<GistLister />, div);
});

it('renders and reads GistLister H2 text', () => {
    const wrapper = shallow(<GistLister />);
    const welcome = <h2>Gist Lister</h2>;
    expect(wrapper.contains(welcome)).toEqual(true);
});