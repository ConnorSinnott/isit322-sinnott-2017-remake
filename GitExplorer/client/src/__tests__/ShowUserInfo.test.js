/**
 * Created by spectre on 5/9/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import ShowUserInfo from '../components/ShowUserInfo';
import fieldDefinitions from '../../git-convert/field-definitions.json';

describe('Show User Info mount Test', () => {

    let bodyData = {};

    beforeEach(() => {
        const tempBody = {};
        for (let value of fieldDefinitions) {
            tempBody[value.id] = value.sample;
        }
        bodyData = tempBody;
    });

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<ShowUserInfo
            fields={fieldDefinitions}
            gitUser={bodyData}
            onChange={() => {
            }}
        />, div);
    });

});
