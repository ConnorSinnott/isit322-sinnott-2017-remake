import React, {Component} from "react";
import {connect} from "react-redux";
import {ACTION_FETCH_NOTIFICATIONS} from './actions/actions_Notifications';
import "../css/App.css";

class ShowNotifications extends Component {

    generateDisplay = () => {
        if (this.props.notificationsStore.isFetching) {
            return <h3> Fetching... </h3>
        } else {
            if (this.props.notificationsStore.notifications) {
                return <pre>{JSON.stringify(this.props.notificationsStore.notifications, null, 4)}</pre>
            } else {
                return <h3> No Data </h3>;
            }
        }
    };

    render() {
        return (
            <div>
                {this.generateDisplay()}
                <button onClick={() => this.props.dispatch(ACTION_FETCH_NOTIFICATIONS)}>Get
                    Notifications
                </button>
            </div>
        );
    }

}

export default connect((state) => {
    return {
        notificationsStore: state.notificationStore
    }
})(ShowNotifications);