import React, {Component} from 'react';
import '../css/App.css';
import {connect} from 'react-redux';
import numbers from '../../config/numbers.json';
import {ACTION_GET_NUMBER} from './actions/actions_GetNumbers';

class SmallNumbers extends Component {

    render() {
        let numberProps = this.props.numbersStore;
        return (
            <div>
                {Object.keys(numberProps).map((name, index) => {
                    return <p key={index}>{name}: {numberProps[name]} </p>;
                })}
                {numbers.map((value, index) => {
                    return <button key={index}
                                   onClick={() => this.props.dispatch(ACTION_GET_NUMBER(index + 1))}>
                        {value}
                    </button>;
                })}
            </div>
        );
    }

}

//wew lads here we go cracking open a cold one with the boys
export default connect((state) => {
    return {
        numbersStore: state.numbersStore
    }
})(SmallNumbers);

