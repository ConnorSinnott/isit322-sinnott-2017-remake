/**
 * Created by spectre on 5/21/17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    ACTION_FETCH_GISTS,
    ACTION_NEXT_GIST,
    ACTION_PREVIOUS_GIST,
    ACTION_DELETE_GIST
} from './actions/actions_Gists';

class GistLister extends Component {

    generateDisplay() {

        if (this.props.gistsStore.isFetching) {
            return <h3> Fetching... </h3>
        }

        if (this.props.gistsStore.gists && this.props.gistsStore.gists.length > 0) {
            let currentGist = this.props.gistsStore.gists[this.props.gistsStore.displayIndex];
            return (
                <div className="panel panel-default">
                    <div className="row">
                        <div className="col-md-8">
                            <h4>
                                [{this.props.gistsStore.displayIndex + 1}/{this.props.gistsStore.gists.length}] {currentGist.description}
                            </h4>
                        </div>
                        <div className="col-md-4">
                            <button className="btn btn-warning" onClick={() => {
                                this.props.dispatch(ACTION_PREVIOUS_GIST);
                            }}>
                                Previous
                            </button>
                            <button className="btn btn-warning" onClick={() => {
                                this.props.dispatch(ACTION_NEXT_GIST);
                            }}>
                                Next
                            </button>
                            <button className="btn btn-danger" onClick={() => {
                                this.props.dispatch(ACTION_DELETE_GIST(currentGist.id));
                            }}>
                                Delete
                            </button>
                        </div>
                    </div>
                    <hr/>
                    <ul>
                        {Object.keys(currentGist).map((key) => {
                            return (
                                <li key={key}>
                                    <p>{key} : {currentGist[key]} </p>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            )
        } else {
            return <h3> No Data </h3>;
        }
    }

    render() {
        return (
            <div>
                {this.generateDisplay()}
                <button className='getGistList' onClick={() => {
                    this.props.dispatch(ACTION_FETCH_GISTS);
                }}>Get GistList
                </button>
            </div>
        );
    }

}

export default connect((state) => {
    return {
        gistsStore: state.gistsStore
    }
})(GistLister);
