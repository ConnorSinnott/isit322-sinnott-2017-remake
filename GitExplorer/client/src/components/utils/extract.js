/**
 * Created by spectre on 6/5/17.
 */
export default (object, columns) => {
    let out = {};
    columns.forEach((item) => {
        out[item] = object[item];
    });
    return out;
}