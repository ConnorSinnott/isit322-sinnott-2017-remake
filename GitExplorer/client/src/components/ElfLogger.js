/**
 * Created by charlie on 4/18/17.
 */
const ElfLogger = class {
    constructor(initQuiet) {
        this.display = initQuiet;
        this.log = this.log.bind(this);
        this.setQuiet = this.setQuiet.bind(this);
    }

    log(message1, message2 = '', message3 = '') {
        if (this.display) {
            console.log(message1, message2, message3);
        }
    };

    setQuiet(newValue) {
        this.display = newValue;
    }
};

export default ElfLogger;
