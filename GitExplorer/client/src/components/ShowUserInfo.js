import React, {Component} from 'react';
import '../css/App.css';
import ElfElements from './ElfElements';
import {ACTION_FETCH_USER} from './actions/actions_User';
import {connect} from 'react-redux';
import fields from "../../git-convert/field-definitions.json";

class ShowUserInfo extends Component {

    getForm = (field) => {
        let value = '';
        if (this.props.userStore.userData) {
            value = this.props.userStore.userData[field.id];
        }
        return (
            <div className="ElfFormRow" key={field.id}>
                <label className="ElfFormLabel" htmlFor={field.id}>{field.label}:</label>
                <ElfElements {...field} value={value}/>
            </div>
        )
    };

    render() {
        return (
            <form className="Form">{
                fields.map((field, index) => {
                    return this.getForm(field, index)
                })
            }
                <button onClick={(e) => {
                    e.preventDefault();
                    this.props.dispatch(ACTION_FETCH_USER)
                }}>Get Git User
                </button>
            </form>
        );
    }
}

export default connect((state) => {
    return {
        userStore: state.userStore
    }
})(ShowUserInfo)