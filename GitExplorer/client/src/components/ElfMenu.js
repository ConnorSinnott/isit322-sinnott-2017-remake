/**
 * Created by spectre on 5/25/17.
 */
import React, {Component} from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

class ElfMenu extends Component {

    render() {
        return (
            <Navbar inverse>
                <Navbar.Brand>
                    <LinkContainer to='/'><NavItem>Home</NavItem></LinkContainer>
                </Navbar.Brand>
                <Nav>
                    <LinkContainer to='/get-gist'>
                        <NavItem>Gist</NavItem></LinkContainer>
                    <LinkContainer to='/get-notifications'>
                        <NavItem>Notifications</NavItem></LinkContainer>
                    <LinkContainer to='/gist-lister'>
                        <NavItem>GistLister</NavItem></LinkContainer>
                    <LinkContainer to='/get-foo'>
                        <NavItem>BarFoo</NavItem></LinkContainer>
                    <LinkContainer to='/get-numbers'>
                        <NavItem>SmallNumbers</NavItem></LinkContainer>
                </Nav>
            </Navbar>
        );
    }

}

export default ElfMenu;