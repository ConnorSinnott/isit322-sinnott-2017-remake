/**
 * Created by spectre on 5/9/17.
 */
import React, {Component} from 'react';
import logo from '../logo.svg';
import {Jumbotron} from 'react-bootstrap';

import ElfMenu from './ElfMenu';

class Header extends Component {

    render() {
        return (
            <div className='App'>
                <ElfMenu/>
                <Jumbotron>
                    <img src={logo} className='App-logo' alt='logo'/>
                    <h2>Welcome to React</h2>
                </Jumbotron>
            </div>
        );
    }

}

export default Header;
