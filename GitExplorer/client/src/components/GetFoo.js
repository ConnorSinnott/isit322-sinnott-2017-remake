import React, {Component} from 'react';
import {ACTION_FETCH_FOO} from './actions/actions_Foo';
import {connect} from 'react-redux';
import '../css/App.css';

class GetFoo extends Component {

    generateDisplay = () => {
        if (this.props.fooStore.isFetching) {
            return <h3> Fetching... </h3>
        } else {
            if (this.props.fooStore.fooData) {
                return (<div>
                    <p className='App-intro'>
                        state.foo: {this.props.fooStore.fooData.foo}
                    </p>
                    <p className='App-intro'>
                        state.file: {this.props.fooStore.fooData.file}
                    </p>
                </div>)
            } else {
                return <h3> No Data </h3>;
            }
        }
    };

    render() {
        return (
            <div className='App'>
                {this.generateDisplay()}
                <button className='getFoo' onClick={() => {
                    this.props.dispatch(ACTION_FETCH_FOO);
                }}>getFoo
                </button>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        fooStore: state.fooStore
    }
}

export default connect(mapStateToProps)(GetFoo);