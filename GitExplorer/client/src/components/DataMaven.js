import React, {Component} from "react";
import "../css/App.css";
import "whatwg-fetch";
import SmallNumbers from "./SmallNumbers";
import GetFoo from "./GetFoo";
import ElfHeader from "./ElfHeader";
import {BrowserRouter as Router, Route} from "react-router-dom";
import ShowUserInfo from "./ShowUserInfo";
import ShowNewGist from "./ShowNewGist";
import ShowNotifications from "./ShowNotifications";
import GistLister from "./GistLister";
import {connect} from "react-redux";

class DataMaven extends Component {

    render() {
        return (
            <Router >
                <div className="App">
                    <ElfHeader/>
                    <Route exact path="/" component={ShowUserInfo}/>
                    <Route exact path="/get-gist" component={ShowNewGist}/>
                    <Route exact path="/gist-lister" component={GistLister}/>
                    <Route exact path="/get-notifications" component={ShowNotifications}/>
                    <Route exact path="/get-foo" component={GetFoo}/>
                    <Route exact path="/get-numbers" component={SmallNumbers}/>
                </div>
            </Router>
        );
    }

}

export default connect()(DataMaven);