import React, {Component} from "react";
import "../css/App.css";
import {connect} from 'react-redux';
import extract from "./utils/extract";
import {ACTION_FETCH_NEW_GIST} from './actions/actions_NewGist';

class ShowNewGist extends Component {

    generateDisplay = () => {

        if (this.props.newGistStore.isFetching)
            return <h3> Fetching... </h3>;

        if (this.props.newGistStore.newGist) {
            let output = extract(this.props.newGistStore.newGist, ['url', 'id', 'public', 'comments', 'truncated']);
            return Object.keys(output).map((key) => {
                let value = output[key];
                return <p key={key}> {key} : {value} </p>
            });
        } else {
            return <h3> No Data </h3>;
        }

    };

    render() {
        return (
            <div>
                {this.generateDisplay()}
                <button className="getUser" onClick={() => {
                    this.props.dispatch(ACTION_FETCH_NEW_GIST);
                }}>Get Gist
                </button>
            </div>
        );
    }

}

export default connect((state) => {
    return {
        newGistStore: state.newGistStore
    }
})(ShowNewGist);