/**
 * Created by spectre on 6/7/17.
 */

export const TYPE_GET_NUMBER = 'TYPE_GET_NUMBER';
export function ACTION_GET_NUMBER(number) {
    return {
        type: TYPE_GET_NUMBER,
        payload: number,
    }
}