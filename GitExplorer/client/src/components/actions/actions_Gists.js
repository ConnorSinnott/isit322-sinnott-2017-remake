/**
 * Created by spectre on 6/6/17.
 */
import 'whatwg-fetch';

export const TYPE_FETCH_GISTS_START = 'TYPE_FETCH_GISTS_START';
export const TYPE_FETCH_GISTS_ERROR = 'TYPE_FETCH_GISTS_ERROR';
export const TYPE_FETCH_GISTS_COMPLETE = 'TYPE_FETCH_GISTS_COMPLETE';
export const TYPE_NEXT_GIST = 'TYPE_NEXT_GIST';
export const TYPE_PREV_GIST = 'TYPE_PREV_GIST';
export const TYPE_DELETE_GIST_START = 'TYPE_DELETE_GIST_START';
export const TYPE_DELETE_GIST_COMPLETE = 'TYPE_DELETE_GIST_COMPLETE';
export const TYPE_DELETE_GIST_ERROR = 'TYPE_DELETE_GIST_ERROR';

export const ACTION_NEXT_GIST = {
    type: TYPE_NEXT_GIST
};
export const ACTION_PREVIOUS_GIST = {
    type: TYPE_PREV_GIST
};

export function ACTION_FETCH_GISTS(dispatch) {
    dispatch({type: TYPE_FETCH_GISTS_START});
    fetch('/gist/gist-list').then((body) => {
        return body.json();
    }).then(function (json) {
        dispatch({type: TYPE_FETCH_GISTS_COMPLETE, payload: json});
    }).catch(function (err) {
        dispatch({type: TYPE_FETCH_GISTS_ERROR, payload: err});
    });
}

export function ACTION_DELETE_GIST(id) {
    return (dispatch) => {
        dispatch({type: TYPE_DELETE_GIST_START});
        fetch('/gist/delete?id=' + id).then(() => {
            dispatch({type: TYPE_DELETE_GIST_COMPLETE, payload: id});
        }).catch(function (err) {
            dispatch({type: TYPE_DELETE_GIST_ERROR, payload: err});
        });
    }
}