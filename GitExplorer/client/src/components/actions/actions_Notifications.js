/**
 * Created by spectre on 6/6/17.
 */
import 'whatwg-fetch';

export const TYPE_FETCH_NOTIFICATIONS_START = 'TYPE_FETCH_NOTIFICATIONS_START';
export const TYPE_FETCH_NOTIFICATIONS_ERROR = 'TYPE_FETCH_NOTIFICATIONS_ERROR';
export const TYPE_NOTIFICATIONS_COMPLETE = 'TYPE_FETCH_NOTIFICATIONS_COMPLETE';
export function ACTION_FETCH_NOTIFICATIONS(dispatch) {
    dispatch({type: TYPE_FETCH_NOTIFICATIONS_START});
    fetch('/git-user/notifications').then((body) => {
        return body.json();
    }).then((json) => {
        dispatch({type: TYPE_NOTIFICATIONS_COMPLETE, payload: json});
    }).catch((err) => {
        console.log('ERR');
        dispatch({type: TYPE_FETCH_NOTIFICATIONS_ERROR, payload: err});
    });
}