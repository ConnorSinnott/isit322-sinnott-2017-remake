/**
 * Created by spectre on 6/7/17.
 */
import 'whatwg-fetch';

export const TYPE_FETCH_NEW_GIST_START = 'TYPE_FETCH_NEW_GIST_START';
export const TYPE_FETCH_NEW_GIST_COMPLETE = 'TYPE_FETCH_NEW_GIST_COMPLETE';
export const TYPE_FETCH_NEW_GIST_ERROR = 'TYPE_FETCH_NEW_GIST_ERROR';
export function ACTION_FETCH_NEW_GIST (dispatch) {
    dispatch({type: TYPE_FETCH_NEW_GIST_START});
    fetch('/gist/gist-test').then((body) => {
        return body.json();
    }).then(function (json) {
        dispatch({type: TYPE_FETCH_NEW_GIST_COMPLETE, payload: json.result});
    }).catch(function (err) {
        dispatch({type: TYPE_FETCH_NEW_GIST_ERROR, payload: err});
    })
}