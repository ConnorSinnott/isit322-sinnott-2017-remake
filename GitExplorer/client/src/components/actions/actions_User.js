/**
 * Created by spectre on 6/6/17.
 */
import 'whatwg-fetch';

export const TYPE_FETCH_USER_START = 'TYPE_FETCH_USER_START';
export const TYPE_FETCH_USER_ERROR = 'TYPE_FETCH_USER_ERROR';
export const TYPE_FETCH_USER_COMPLETE = 'TYPE_FETCH_USER_COMPLETE';
export function ACTION_FETCH_USER(dispatch) {
    dispatch({type: TYPE_FETCH_USER_START});
    fetch('/git-user/user').then((body) => {
        return body.json();
    }).then((json) => {
        if (json.err) {
            throw new Error(json.err);
        } else {
            dispatch({type: TYPE_FETCH_USER_COMPLETE, payload: JSON.parse(json.body)});
        }
    }).catch((err) => {
        dispatch({type: TYPE_FETCH_USER_ERROR, payload: err});
    });
}
