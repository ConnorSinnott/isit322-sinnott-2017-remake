/**
 * Created by spectre on 6/7/17.
 */
import 'whatwg-fetch';

export const TYPE_FETCH_FOO_START = 'TYPE_FETCH_FOO_START';
export const TYPE_FETCH_FOO_ERROR = 'TYPE_FETCH_FOO_ERROR';
export const TYPE_FETCH_FOO_COMPLETE = 'TYPE_FETCH_FOO_COMPLETE';
export function ACTION_FETCH_FOO(dispatch) {
    dispatch({type: TYPE_FETCH_FOO_START});
    fetch('/foo').then((body) => {
        return body.json();
    }).then(function (json) {
        dispatch({type: TYPE_FETCH_FOO_COMPLETE, payload: json});
    }).catch(function (err) {
        dispatch({type: TYPE_FETCH_FOO_ERROR, payload: err});
    })
};