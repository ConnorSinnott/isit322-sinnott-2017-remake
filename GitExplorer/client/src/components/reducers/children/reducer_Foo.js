/**
 * Created by spectre on 6/7/17.
 */

import {
    TYPE_FETCH_FOO_COMPLETE,
    TYPE_FETCH_FOO_START,
    TYPE_FETCH_FOO_ERROR
} from '../../actions/actions_Foo';

const defaultState = {
    isFetching: false,
    fooData: null,
    error: null
};

export default function (state = defaultState, action) {
    state = {...state, error: null};
    switch (action.type) {
        case TYPE_FETCH_FOO_START:
            return {...state, isFetching: true, fooData: null};
        case TYPE_FETCH_FOO_COMPLETE:
            return {...state, isFetching: false, fooData: action.payload};
        case TYPE_FETCH_FOO_ERROR:
            return {...state, isFetching: false, error: action.payload};
        default:
            return state;
    }
}