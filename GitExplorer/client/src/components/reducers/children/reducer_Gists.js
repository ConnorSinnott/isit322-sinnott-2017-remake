/**
 * Created by spectre on 6/6/17.
 */
import {
    TYPE_FETCH_GISTS_START,
    TYPE_FETCH_GISTS_COMPLETE,
    TYPE_FETCH_GISTS_ERROR,
    TYPE_PREV_GIST,
    TYPE_NEXT_GIST,
    TYPE_DELETE_GIST_START,
    TYPE_DELETE_GIST_COMPLETE,
    TYPE_DELETE_GIST_ERROR
} from '../../actions/actions_Gists';

import {
    TYPE_FETCH_NEW_GIST_COMPLETE
} from '../../actions/actions_NewGist';

const defaultState = {
    isFetching: false,
    isDeleting: false,
    gists: null,
    error: null,
    deleteError: null,
    displayIndex: null
};

export default (state = defaultState, action) => {
    state = {...state, error: null, deleteError: null};

    switch (action.type) {

        case TYPE_FETCH_GISTS_START:
            return {...state, isFetching: true, gists: null};

        case TYPE_FETCH_GISTS_COMPLETE:
            return {...state, isFetching: false, gists: action.payload, displayIndex: 0};

        case TYPE_FETCH_GISTS_ERROR:
            return {...state, isFetching: false, gists: null, error: action.payload};

        case TYPE_NEXT_GIST: {
            let displayIndex = state.displayIndex;
            if (displayIndex < state.gists.length - 1) {
                return {...state, displayIndex: displayIndex + 1}
            } else {
                return state;
            }
        }

        case TYPE_PREV_GIST: {
            let displayIndex = state.displayIndex;
            if (displayIndex > 0) {
                return {...state, displayIndex: displayIndex - 1}
            } else {
                return state;
            }
        }

        case TYPE_DELETE_GIST_START:
            return {...state, isDeleting: true};

        case TYPE_DELETE_GIST_COMPLETE: {
            let newState = {...state, isDeleting: false};
            newState.gists = newState.gists.filter((obj) => {
                return obj.id !== action.payload;
            });
            if (newState.displayIndex === state.gists.length - 1) {
                newState.displayIndex--;
            }
            return newState;
        }

        case TYPE_DELETE_GIST_ERROR:
            return {...state, isDeleting: false, deleteError: action.payload};

        case TYPE_FETCH_NEW_GIST_COMPLETE:
            return defaultState;

        default:
            return state;
    }
};
