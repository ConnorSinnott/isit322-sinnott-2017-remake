/**
 * Created by spectre on 6/7/17.
 */

import {
    TYPE_FETCH_NEW_GIST_ERROR,
    TYPE_FETCH_NEW_GIST_COMPLETE,
    TYPE_FETCH_NEW_GIST_START
} from '../../actions/actions_NewGist';

const defaultState = {
    isFetching: false,
    newGist: null,
    error: null
};

export default (state = defaultState, action) => {
    state = {...state, error: null};
    switch (action.type) {
        case TYPE_FETCH_NEW_GIST_START:
            return {...state, isFetching: true, newGist: null};
        case TYPE_FETCH_NEW_GIST_COMPLETE:
            return {...state, isFetching: false, newGist: action.payload};
        case TYPE_FETCH_NEW_GIST_ERROR:
            return {...state, isFetching: false, error: action.payload};
        default:
            return state;
    }
}