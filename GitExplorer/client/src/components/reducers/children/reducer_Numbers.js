/**
 * Created by spectre on 6/6/17.
 */
import {TYPE_GET_NUMBER} from '../../actions/actions_GetNumbers';
import numbers from '../../../../config/numbers.json';

let defaultState = {};
numbers.forEach((value) => {
    defaultState[value] = 0;
});

export default (state = defaultState, action) => {

    if (action.type === TYPE_GET_NUMBER) {
        let stateChange = {};
        stateChange[numbers[action.payload - 1]] = action.payload;
        state = {...state, ...stateChange}
    }

    return state;

}
