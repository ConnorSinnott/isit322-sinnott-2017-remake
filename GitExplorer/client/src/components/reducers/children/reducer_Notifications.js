/**
 * Created by spectre on 6/6/17.
 */
import {
    TYPE_FETCH_NOTIFICATIONS_START,
    TYPE_NOTIFICATIONS_COMPLETE,
    TYPE_FETCH_NOTIFICATIONS_ERROR
} from '../../actions/actions_Notifications';

const defaultState = {
    isFetching: false,
    notifications: null,
    error: null
};

export default (state = defaultState, action) => {
    state = {...state, error: null};
    switch (action.type) {
        case TYPE_FETCH_NOTIFICATIONS_START:
            return {...state, isFetching: true, notifications: null};
        case TYPE_NOTIFICATIONS_COMPLETE:
            return {...state, isFetching: false, notifications: action.payload};
        case TYPE_FETCH_NOTIFICATIONS_ERROR:
            return {...state, isFetching: false, notifications: null, error: action.payload};
        default:
            return state;
    }
}
