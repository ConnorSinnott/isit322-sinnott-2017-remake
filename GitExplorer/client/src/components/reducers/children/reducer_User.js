/**
 * Created by spectre on 6/6/17.
 */
import {
    TYPE_FETCH_USER_START,
    TYPE_FETCH_USER_COMPLETE,
    TYPE_FETCH_USER_ERROR
} from '../../actions/actions_User';

const defaultState = {
    isFetching: false,
    userData: null,
    error: null
};

export default (state = defaultState, action) => {
    state = {...state, error: null};
    switch (action.type) {
        case TYPE_FETCH_USER_START:
            return {...state, isFetching: true, userData: null};
        case TYPE_FETCH_USER_COMPLETE:
            return {...state, isFetching: false, userData: action.payload};
        case TYPE_FETCH_USER_ERROR:
            return {...state, isFetching: false, userData: null, error: action.payload};
        default:
            return state;
    }
}
