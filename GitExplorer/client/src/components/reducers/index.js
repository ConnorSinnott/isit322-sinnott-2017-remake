/**
 * Created by spectre on 6/6/17.
 */
import {combineReducers} from 'redux';
import ReducerGists from './children/reducer_Gists';
import ReducerNewGist from './children/reducer_NewGist';
import ReducerNotifications from './children/reducer_Notifications';
import ReducerNumbers from './children/reducer_Numbers';
import ReducerUser from './children/reducer_User';
import ReducerFoo from './children/reducer_Foo';

export default combineReducers({
    gistsStore: ReducerGists,
    notificationStore: ReducerNotifications,
    numbersStore: ReducerNumbers,
    userStore: ReducerUser,
    fooStore: ReducerFoo,
    newGistStore: ReducerNewGist
});