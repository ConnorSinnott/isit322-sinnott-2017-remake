import React from 'react';
import ReactDOM from 'react-dom';
import DataMaven from './components/DataMaven';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap/dist/css/bootstrap-theme.css';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import allReducers from './components/reducers';

const logger = createLogger({
    collapsed: true
});

const middleware = applyMiddleware(thunk, logger);
const store = createStore(allReducers, middleware);

ReactDOM.render(
    <Provider store={store}>
        <div className='container'>
            <DataMaven/>
        </div>
    </Provider>,
    document.getElementById('root')
);
