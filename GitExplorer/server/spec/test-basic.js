/**
 * Created by spectre on 5/25/17.
 */
var request = require('supertest');
var app = require('../app');

describe('Elvenware Simple Plain Suite', function () {

    'use strict';

    it('expects true to be true', function () {
        expect(true).toBe(true);
    });

    it('get the foo route', function (done) {
        request(app)
            .get('/api/foo')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                done();
            });
    });

    it('gets the basic gists list', function (done) {
        request(app)
            .get('/api/gist-list')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                done();
            });
    });

    it('checks the gist response', function (done) {
        request(app)
            .get('/api/gist-list')
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(function (response) {
                const gist = response.body[0];
                expect(gist.url).toBeDefined();
                expect(gist.id).toBeDefined();
                expect(gist.description).toBeDefined();
                expect(gist.login).toBeDefined();
                expect(gist.type).toBeDefined();
            })
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                done();
            });
    });

});
