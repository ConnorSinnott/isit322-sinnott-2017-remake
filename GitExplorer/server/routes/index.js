/**
 * Created by spectre on 6/5/17.
 */
let express = require('express');
let router = express.Router();
let requester = require('request');
let URL = require('url');

function buildRedirect(req, port) {
    let url = URL.parse(req.url);
    let path = '/' + url.path.split('/').splice(2).join('/');
    let request = 'http://localhost:' + port + path;
    if (url.search) {
        request += url.search;
    }
    return request;
}

router.get('/', (req, res) => {
});

router.get('/foo', (req, res) => {
    res.status(200).json({'result': 'success', 'foo': 'bar', 'file': 'api.js'});
});

router.get('/qux/**', (req, res) => {
    requester(buildRedirect(req, 30031)).pipe(res);
});

router.get('/git-user/**', (req, res) => {
    requester(buildRedirect(req, 30029)).pipe(res);
});

router.get('/gist/**', (req, res) => {
    requester(buildRedirect(req, 30027)).pipe(res);
});

router.get('/markdown/**', (req, res) => {
    requester(buildRedirect(req, 30030)).pipe(res);
});

router.get('/git-socket/**', (req, res) => {
    requester(buildRedirect(req, 30028)).pipe(res);
});

module.exports = router;
