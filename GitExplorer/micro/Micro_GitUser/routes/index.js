/**
 * Created by spectre on 6/5/17.
 */
let express = require('express');
let router = express.Router();

let request = require('request');
let GitHub = require('github-api');

router.get('/', (req, res) => {
    res.status(200).json({status: 'ok'});
});


router.get('/user', (req, res) => {

    request({
        url: 'https://api.github.com/users/connorsinnott',
        headers: {
            'User-Agent': 'request'
        }
    }, function (error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        console.log('body:', body);
        res.status(200).json({error: error, response: response, body: body});
    });

});

router.get('/notifications', (req, res) => {

    let getGitHub = function () {
        let gh;
        if (true) {
            gh = new GitHub({
                token: process.env.TOKEN
            });
        } else {
            gh = new GitHub({
                username: 'charliecalvert',
                password: ''
            });
        }
        return gh;
    };

    let me = getGitHub().getUser(); // no user specified defaults to the user for whom credentials were provided
    me.listNotifications(function (err, notifications) {
        if (!err) {
            res.status(200).json(notifications);
        } else {
            res.status(500).json(err);
        }
    });

});


module.exports = router;