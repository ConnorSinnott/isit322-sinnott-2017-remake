/**
 * Created by spectre on 5/9/17.
 */
let express = require('express');
let router = express.Router();

let request = require('request');
let GitHub = require('github-api');

getGitHub = function () {
    let gh;
    if (true) {
        gh = new GitHub({
            token: process.env.TOKEN
        });
    } else {
        gh = new GitHub({
            username: 'charliecalvert',
            password: ''
        });
    }
    return gh;
};


router.get('/', (req, res) => {
    res.status(200).json({
        status: 'ok'
    });
});

router.get('/foo', (req, res) => {
    res.status(200).json({'result': 'success', 'foo': 'bar', 'file': 'api.js'});
});

router.get('/gist-test', (req, res) => {

    const gh = getGitHub();
    let gist = gh.getGist(); // not a gist yet
    gist.create({
        public: true,
        description: 'My first gist',
        files: {
            'file1.txt': {
                content: 'Gists!'
            }
        }
    }).then(function ({data}) {
        // Promises!
        let createdGist = data;
        return gist.read();
    }).then(function ({data}) {
        let retrievedGist = data;
        console.log('Retrieved: ', retrievedGist);
        res.status(200).json({'result': retrievedGist});
        // do interesting things
    }).catch(function (err) {
        console.log('Rejected: ', err);
        res.status(500).json({'result': err});
    });

});

router.get('/gist-list', (req, res) => {

    let me = getGitHub().getUser(); // no user specified defaults to the user for whom credentials were provided
    me.listGists(function (err, gists) {
        if (!err) {
            res.status(200).json(gists.map((value) => {
                return {
                    url: value.url,
                    id: value.id,
                    description: value.description,
                    login: value.owner.login,
                    type: value.owner.type
                }
            }));
        } else {
            res.status(500).json(err);
        }
    });

});

router.get('/delete', (req, res) => {
    getGitHub().getGist(req.query.id).delete((err, result) => {
        if (!err) {
            res.status(200).send(result);
        } else {
            res.status(500).send(err);
        }
    });
});

module.exports = router;
