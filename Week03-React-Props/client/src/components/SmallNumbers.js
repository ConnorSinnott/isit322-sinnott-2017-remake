import React, {Component} from 'react';
import '../css/App.css';
import numbers from '../../config/numbers.json'

class SmallNumbers extends Component {

    constructor(props) {
        super();
        this.state = {};
        numbers.forEach((name, i) => {
            this['get' + name] = () => {
                let stateChange = {};
                stateChange[name] = i + 1;
                this.setState(stateChange);
            };
            this.state[name] = props.numbers ? props.numbers[name] : 0;
        });
    }

    render() {
        return (
            <div className="App">
                {numbers.map((name) => {
                    return (
                        <p className="App-intro">
                            state.{name}: {this.state[name]}
                        </p>
                    )
                })}
                {numbers.map((name) => {
                    return (
                        <button className={'get' + name} onClick={this['get' + name]}> get{name} </button>
                    )
                })}
            </div>
        );
    }
}

export default SmallNumbers;

