import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import SmallNumbers from './components/SmallNumbers'
import Header from './components/Header'
import './index.css';

import numbers from '../config/numbers.json'

let numbersInit = {};
for (let index in numbers) {
    let name = numbers[index];
    numbersInit[name] = Math.floor(Math.random() * 10);
}

ReactDOM.render(
    (<div>
        <Header/>
        <App />
        <SmallNumbers numbers={numbersInit}/>
    </div>),
    document.getElementById('root')
);
