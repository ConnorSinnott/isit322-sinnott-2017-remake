/**
 * Created by spectre on 5/9/17.
 */
let express = require('express');
let router = express.Router();

let request = require('request');

router.get('/foo', (req, res) => {
    res.status(200).json({'result': 'success', 'foo': 'bar', 'file': 'api.js'});
});

// EXISTING CODE OMITTED HERE
router.get('/user', (req, res, next) => {

    request({
        url: 'https://api.github.com/users/connorsinnott',
        headers: {
            'User-Agent': 'request'
        }
    }, function (error, response, body) {
        // Print the error if one occurred
        console.log('error:', error);
        // Print the response status code if a response was received
        console.log('statusCode:', response && response.statusCode);
        // Print the HTML for the Google homepage.
        console.log('body:', body);
        res.status(200).json({error: error, response: response, body: body});
    });

});


module.exports = router;

