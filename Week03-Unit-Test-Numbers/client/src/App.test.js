import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow} from 'enzyme';

import numbers from '../config/numbers.json'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

for(let index = 0; index < numbers.length; index++) {
    let name = numbers[index];

    it('renders initial value of paragraph with state.'+name, () => {
        const wrapper = shallow(<App />);
        const nineSign = <p className="App-intro">state.{name}: 0</p>;
        // const ninep = wrapper.find('p').last().debug();
        // console.log(ninep);
        expect(wrapper.contains(nineSign)).toEqual(true);
    });

    it('renders button click message for '+name, () => {
        const wrapper = shallow(<App />);
        const nineSign = <p className="App-intro">state.{name}: {index + 1}</p>;
        wrapper.find('button.get'+name).simulate('click');
        expect(wrapper.contains(nineSign)).toEqual(true);
    });

}


