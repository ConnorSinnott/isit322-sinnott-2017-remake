import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import 'whatwg-fetch';
import numbers from '../config/numbers.json'

class App extends Component {

    constructor() {
        super();
        this.state = {
            file: 'Get Nine Result will be placed here.',
            foo: 'waiting for server',
        };

        numbers.forEach((name, i) => {
            this.state[name] = 0;
            this['get' + name] = () => {
                let stateChange = {};
                stateChange[name] = i + 1;
                this.setState(stateChange);
            }
        });

    }

    getFoo = () => {
        console.log('GetFoo is called');
        const that = this;
        fetch('/api/foo')
            .then(function (response) {
                console.log('got response');
                return response.json();
            }).then(function (json) {
            console.log('parsed json', json);
            that.setState(foo => (json));
        }).catch(function (ex) {
            console.log('elf parsing failed', ex);
        });
    };

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    state.foo: {this.state.foo}
                </p>
                <p className="App-intro">
                    state.file: {this.state.file}
                </p>
                <button className="getFoo" onClick={this.getFoo}>Bar</button>
                {numbers.map((name) => {
                    return (
                        <p className="App-intro">
                            state.{name}: {this.state[name]}
                        </p>
                    )
                })}
                {numbers.map((name) => {
                    return (
                        <button className={'get' + name} onClick={this['get' + name]}> get{name} </button>
                    )
                })}
            </div>
        );
    }
}

export default App;

