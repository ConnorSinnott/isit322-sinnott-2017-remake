import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow} from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('renders initial value of paragraph with state.nine', () => {
    const wrapper = shallow(<App />);
    const nineSign = <p className="App-intro">state.nine: 0</p>;
    const ninep = wrapper.find('p').last().debug();
    console.log(ninep);
    expect(wrapper.contains(nineSign)).toEqual(true);
});
