/**
 * Created by spectre on 5/9/17.
 */
let express = require('express');
let router = express.Router();

router.get('/foo', (req, res) => {
    res.status(200).json({'result': 'success', 'foo': 'bar', 'file': 'api.js'});
});

module.exports = router;

